import { IEvent } from '../types';

const getItemStringValue = (item: any) => item.S;

export const mapEvent = (event: any): IEvent => {
  const id = getItemStringValue(event.id);
  const name = getItemStringValue(event.name);
  const date = getItemStringValue(event.date);
  const location = getItemStringValue(event.location);
  const expectedWeather = getItemStringValue(event.expectedWeather);

  return {
    id,
    name,
    date,
    location,
    expectedWeather,
  };
};

export const mapEvents = (events: any): IEvent[] => {
  return events.map(mapEvent);
};
