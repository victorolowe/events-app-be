import logger from '../../../logger';
import { AWS } from '../../../services';

const createTable = () => {
  const params = {
    TableName: 'Events',
    KeySchema: [{ AttributeName: 'id', KeyType: 'HASH' }],
    AttributeDefinitions: [{ AttributeName: 'id', AttributeType: 'S' }],
    ProvisionedThroughput: {
      ReadCapacityUnits: 10,
      WriteCapacityUnits: 10,
    },
  };

  AWS.DynamoDB.createTable(params, (err, data) => {
    if (err) {
      logger.error('Unable to create events table', err.message);
    } else {
      logger.info('Created events table', data);
    }
  });
};

createTable();
