import { AWS } from '../../../services';
import { IEventDetail } from '../../../types';

const updateEvent = (id: string, eventDetail: IEventDetail) => {
  const params = {
    TableName: 'Events',
    Item: {
      id: { S: id },
      name: { S: eventDetail.name },
      location: { S: eventDetail.location },
      date: { S: eventDetail.date },
      expectedWeather: { S: eventDetail.expectedWeather },
    },
  };

  return AWS.DynamoDB.putItem(params).promise();
};

export default updateEvent;
