import { v4 as uuidv4 } from 'uuid';
import { AWS } from '../../../services';
import { IEventDetail } from '../../../types';

const addEvent = (eventDetail: IEventDetail) => {
  const params = {
    TableName: 'Events',
    Item: {
      id: { S: uuidv4() },
      name: { S: eventDetail.name },
      location: { S: eventDetail.location },
      date: { S: eventDetail.date },
      expectedWeather: { S: eventDetail.expectedWeather },
    },
  };

  return AWS.DynamoDB.putItem(params).promise();
};

export default addEvent;
