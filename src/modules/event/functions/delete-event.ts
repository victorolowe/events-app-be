import { AWS } from '../../../services';

const deleteEvent = (id: string) => {
  const params = {
    TableName: 'Events',
    Key: {
      id: { S: id },
    },
  };

  return AWS.DynamoDB.deleteItem(params).promise();
};

export default deleteEvent;
