import { AWS } from '../../../services';

const getEvents = () => {
  const params = {
    TableName: 'Events',
  };

  return AWS.DynamoDB.scan(params).promise();
};

export default getEvents;
