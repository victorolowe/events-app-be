import { AWS } from '../../../services';

const getEvent = (id: string) => {
  const params = {
    TableName: 'Events',
    Key: {
      id: { S: id },
    },
  };

  return AWS.DynamoDB.getItem(params).promise();
};

export default getEvent;
