export interface IEventDetail {
  name: string;
  date: any;
  location: string;
  expectedWeather: string;
}

export interface IEvent extends IEventDetail {
  id: string;
}
