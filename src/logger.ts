import winston from 'winston';

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: {
    service: 'events-app-be',
  },
  transports: [],
});

const options: winston.transports.ConsoleTransportOptions = {};

logger.add(new winston.transports.Console(options));

export default logger;
