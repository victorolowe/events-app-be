import './utils/env';
import express from 'express';
import cors from 'cors';
import logger from './logger';
import config from './config';
import { eventModule } from './modules';
import { mapEvents, mapEvent } from './utils';
import { body, validationResult } from 'express-validator';

const bootstrap = async () => {
  const app = express();
  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());
  app.use(cors());

  const port = config.http.port;

  app.get('/events', async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    }
    try {
      const { Items } = await eventModule.getEvents();
      res.status(200).json({ events: mapEvents(Items) });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  });

  app.get('/event/:id', async (req, res) => {
    try {
      const { Item } = await eventModule.getEvent(req.params.id);
      res.status(200).json({ event: mapEvent(Item) });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  });

  app.post(
    '/event',
    body('name').isString().trim().escape(),
    body('location').isString().trim().escape(),
    body('date').isString().trim(),
    body('expectedWeather').isString().trim().escape(),
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      try {
        await eventModule.addEvent(req.body);
        res.status(200).send(true);
      } catch (error) {
        res.status(500).json({
          message: error.message,
        });
      }
    }
  );

  app.put(
    '/event/:id',
    body('name').isString().trim().escape(),
    body('location').isString().trim().escape(),
    body('date').isString().trim(),
    body('expectedWeather').isString().trim().escape(),
    async (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }

      try {
        await eventModule.updateEvent(req.params.id, req.body);
        res.status(200).send(true);
      } catch (error) {
        res.status(500).json({
          message: error.message,
        });
      }
    }
  );

  app.delete('/event/:id', async (req, res) => {
    try {
      await eventModule.deleteEvent(req.params.id);
      res.status(200).send(true);
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  });

  app.listen(port, () => {
    logger.info(`Events app listening at ${port}`);
  });
};

bootstrap();
