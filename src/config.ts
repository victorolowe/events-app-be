const { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, PORT, AWS_REGION } = process.env;

interface Config {
  aws: {
    accessKeyId: string;
    secretAccessKey: string;
    region: string;
  };
  http: {
    port: number;
  };
}

const load = (): Config => ({
  aws: {
    accessKeyId: AWS_ACCESS_KEY_ID,
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
    region: AWS_REGION,
  },
  http: {
    port: parseInt(PORT, 10),
  },
});

const config = load();

export default config;
