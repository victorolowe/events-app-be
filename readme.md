# events-app-be

Events App (backend).

## Technologies

- Built with TypeScript.
- [AWS DynamoDB](https://aws.amazon.com/dynamodb) for storage.
- [AWS Cognito](https://aws.amazon.com/cognito/getting-started/) for authentication.
- Services deployed to [Heroku](https://dashboard.heroku.com/apps).
- Weather Forecast with [Open Weather Map](https://openweathermap.org/api).
- [Geocoding API](https://developers.google.com/maps/documentation/geocoding/overview) for address lookup.

## Setup (Requires [AWS account and CLI](https://docs.aws.amazon.com/comprehend/latest/dg/getting-started.html) )

- Clone the project: `git clone <project-url>`

- Navigate to project directory: `cd <project-directory>`

- Duplicate `.env.example` and rename the new file to `.env`

- Update newly created `.env` with correct values

- Install dependencies: `npm install`

## Important

After installing the dependencies, you need to run `npm run create-table` to setup the database.

## Building and running the app

To start the project: `npm start`

### Linting

To lint the project: `npm run lint:fix`
